import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DELL on 2015-05-30.
 */
public class Contacts extends JFrame{
    private JPanel rootPanel;
    private JTextField text_c_name;
    private JTextField text_c_number;
    private JTextField text_c_group;
    private JTextField text_g_groupname;
    private JButton groupRegisterButton;
    private JButton groupDeleteButton;
    private JButton contactRegisterButton;

    private ContactsCTRL mContactsCTRL;

    public static final String nullname = "미지정";
    public Contacts(){
        super("Phone Book System : Contacts Controller :)");
        pack();
        setContentPane(rootPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //이 JFrame만 close 해줌.

        init();

        setSize(490, 300);
        setVisible(true);
    }

    public void init(){
        mContactsCTRL = new ContactsCTRL();
        //Listener 등록
        //연락처 등록
        contactRegisterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("contact register clicked");
                String name = text_c_name.getText();
                String number = text_c_number.getText();
                String group = text_c_group.getText();
                if(group.equals("")){
                    String noname = "미지정";
                    text_c_group.setText(noname);
                    group = noname;

                }
                debugPrint(name + "\t" + number + "\t" + group);
                try {
                    ContactsModel cm = new ContactsModel(name, number, group);
                    mContactsCTRL.RegisterContact(cm);
                } catch (FormatNotFoundException | CharacterExcessException | InputNotFoundException e1) {
                    JOptionPane.showMessageDialog(null, e1.getMessage());
                }
            }
        });

        //그룹 삭제
        groupDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("group delete clicked");
                String group = text_g_groupname.getText();
                mContactsCTRL.DeleteGroup(group);
            }
        });

        //그룹 등록
        groupRegisterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("group register clicked");
                String group = text_g_groupname.getText();
                mContactsCTRL.RegisterGroup(group);
            }
        });
    }

    private void debugPrint(String str){
        System.err.println("DEBUG:: " + str);
    }
}
