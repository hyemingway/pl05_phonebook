import javax.swing.*;
import java.util.regex.*;

/**
 * Created by DELL on 2015-05-31.
 */
public class ContactsModel {
    private String name;
    private String number;
    private String group;

    public ContactsModel(String name, String number, String group) throws FormatNotFoundException, CharacterExcessException, InputNotFoundException {
        this.name = name;
        this.number = number;
        this.group = group;
        CheckInputNull(this.name);
        CheckInputNull(this.number);
        CheckPattern(this.number);
        CheckName(this.name);
        CheckCharacterExcess(this.name);
        CheckCharacterExcess(this.group);
    }

    public Boolean CheckPattern(String str) throws FormatNotFoundException {
        //전화번호 형식 정규식 체크
        Pattern p = Pattern.compile("^\\d{3}-\\d{3,4}-\\d{4}$");
        Matcher m = p.matcher(str);
        if (m.matches()) {
            return true;
        } else {
            throw new FormatNotFoundException("Number");
//            return false;
        }
    }

    public Boolean CheckName(String name) throws FormatNotFoundException {
        //이름에 한글 또는 영문이 가능하도록 숫자가 입력되면 exception throw
        Pattern p = Pattern.compile("[\\x{ac00}-\\x{d7af}]{2,4}");
        Matcher m = p.matcher(name);
        if(m.matches()){
            return true;
        }else {
            throw new FormatNotFoundException("Name");
        }
    }

    public Boolean CheckCharacterExcess(String str) throws CharacterExcessException {
        //이름 또는 그룹명이 최대 5자가 넘어가는 경우 exception throw
        int num = str.length();
        if (num <= 5) {
            return true;
        } else {
            throw new CharacterExcessException();
        }
    }
    public Boolean CheckInputNull(String str) throws InputNotFoundException {
        if (!str.equals("")) {
            return true;
        } else {
            throw new InputNotFoundException();
        }
    }
    //Getters
    public String GetName() {
        return this.name;
    }
    public String GetRawNumber(){
        String[] tokens = number.split("-");
        String res = tokens[0] + tokens[1] + tokens[2];
        return res;
    }

    public String GetNumber() {
        return this.number;
    }

    public String GetGroup() {
        return this.group;
    }

    public String toString() {
        String res = "";
        res = this.name + "\t" + this.number + "\t" + this.group;
        return res;
    }

}
