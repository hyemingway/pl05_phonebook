import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DELL on 2015-05-22.
 */
public class General extends JFrame{
    private JButton phoneNumberButton;
    private JButton nameSearchButton;
    private JButton numberSearchButton;
    private JButton groupSearchButton;
    private JPanel rootPanel;

    public General(){
        super("Phone Book System :)");
        pack();
        setContentPane(rootPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

//        debugPrint();
        AddListener();

        setSize(560, 300);

        setVisible(true);



    }

    private void AddListener() {
        phoneNumberButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("phoneNum clicked");
                Contacts contacts = new Contacts();
            }
        });

        nameSearchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("nameSearch clicked");
                Search searchNum = new Search("Name");
            }
        });

        numberSearchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("numSearch clicked");
                Search searchNum = new Search("Number");

            }
        });

        groupSearchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("groupSearch clicked");
                Search searchGroup = new Search("Group");
            }
        });


    }

    private void debugPrint(String str){
        System.err.println("DEBUG:: " + str);
    }
}
