import javax.swing.*;
import java.util.*;

/**
 * Created by DELL on 2015-05-31.
 */
public class ContactsCTRL {
    //연락처 등록
    //그룹명 등록
    //그룹명 삭제
    private FileCTRL mFileCTRL;
    private GroupManage mGroupManage;
//    private static ArrayList<String> readyGroups;
    private ArrayList<String> List;

    public ContactsCTRL() {
        mFileCTRL = FileCTRL.getInstance();
        mGroupManage = GroupManage.getInstance();

    }

    public void RegisterContact(ContactsModel cm) {
        //연락처 등록
        //그룹리스트에 있는 그룹만 가능
        try {
            boolean check = mGroupManage.IsExist(cm.GetGroup());
                    //mGroupManage.AddToGroup(cm.GetGroup());
            if(!check) {
                throw new GroupNotFoundException();
            }
            mFileCTRL.AddElement(cm);
            System.err.println(mGroupManage.toString());
        } catch (GroupNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public void RegisterGroup(String Group) {
        //그룹명 등록
        //그냥 그룹명이 등록되는 것임 -> 파일엔 안쓰고..!
        //프로그램 종료까지 등록만하고 파일에 적지않은 그룹명은 없어집니다
        try {
            mGroupManage.AddToGroup(Group);
        } catch (GroupNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    public synchronized void DeleteGroup(String Group) {
        //그룹명 삭제
        //존재하지 않는 그룹은 삭제 안됨 & 메시지 출력
        //그룹명 삭제 -> 미지정으로 바꿔줌
        //그룹 리스트 삭제
        try {
            boolean check = mGroupManage.DeleteGroup(Group);
            if(!check){
                throw new GroupNotFoundException();
            }
            ArrayList<ContactsModel> list = mFileCTRL.getmList();

            for(int i = 0; i < list.size(); i++){
                ContactsModel cm = list.get(i);
                boolean equals = cm.GetGroup().equals(Group);
                if(equals){
                    ContactsModel after = new ContactsModel(cm.GetName(),cm.GetNumber(),Contacts.nullname);
                mFileCTRL.ModifyElement(cm, after);
                }
            }
        } catch (GroupNotFoundException | CharacterExcessException | InputNotFoundException | FormatNotFoundException e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
}
