import java.io.*;
import java.util.*;
/**
 * Created by DELL on 2015-06-02.
 */
public class GroupManage {
    private static GroupManage SingleGroupManage;
    private ArrayList<String> groupList;

    private GroupManage(){
        //const
        groupList = new ArrayList<>();
        this.LoadGroup();
        this.debugPrint();
    }
    public static GroupManage getInstance(){
        //Singleton Pattern
        if(SingleGroupManage == null){
            SingleGroupManage = new GroupManage();
        }
        return SingleGroupManage;
    }

    private void debugPrint() {
        String debug = "";
        for (String str : groupList) {
            debug += str + "\n";
        }
        System.err.println("DEBUG:: " + debug);
    }


    //그룹 추가
    public boolean AddToGroup(String gName) throws GroupNotFoundException {
        if(!IsExist(gName)){
            groupList.add(gName);
            debugPrint();
            return true;
        }
        return false;
    }
    //그룹 삭제
    public boolean DeleteGroup(String gName) throws GroupNotFoundException {
        if (IsExist(gName)) {
            groupList.remove(gName);
            return true;
        }
        return false;
    }
    //
    public boolean IsExist(String gName) throws GroupNotFoundException {
        if(groupList.contains(gName)){
            return true;
        }
        return false;
//        throw new GroupNotFoundException();
    }

    private void LoadGroup(){
        //from File
        BufferedReader br = null;
        try {
            File file = new File("data.txt");
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"euc-kr"));
            while (true) {
                String line = null;
                try {
                    line = br.readLine();
                    if (line == null) break;
                    String temp = TokenGroup(line);
//                    System.err.println("DEBUG:: " + temp);
                    groupList.add(temp);


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }//while

            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String TokenGroup(String line){
        String[] tokens;
        tokens = line.split("\t");

        if(tokens.length == 3){
          return tokens[2];
        }else return "";
    }

    public String toString(){
        String res = "";
        for(String str : groupList){
            res += str + "\n";
        }
        return res;
    }
}
