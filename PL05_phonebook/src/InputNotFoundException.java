/**
 * Created by DELL on 2015-05-31.
 */
public class InputNotFoundException extends Exception {
    public InputNotFoundException(){
        super("Input is not found.");
    }
}
