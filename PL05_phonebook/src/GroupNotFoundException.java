/**
 * Created by DELL on 2015-05-31.
 */
public class GroupNotFoundException extends Exception {
    public GroupNotFoundException(){
        super("Group is not found.");
    }
}
