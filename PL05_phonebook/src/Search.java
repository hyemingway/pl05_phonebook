import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by DELL on 2015-05-31.
 */
public class Search extends JFrame {
    private JPanel rootPanel;
    private JTextField text_search;
    private JButton cancelButton;
    private JButton searchButton;
    private JTable table1;
    private JButton sendSMSButton;
    private JButton modifyButton;
    private JButton deleteButton;
    private JTextArea textArea1;

    private SearchCTRL mSearchCTRL;
    //    private SearchCTRL nameSearch;
//    private SearchCTRL numberSearch;
//    private SearchCTRL groupSearch;
    private String SearchType;

    //for Table setting
    private Vector<String> userColumn;
    private DefaultTableModel model;

    private boolean stopflag;
//    private Vector<String> userRow;

    public Search(String type) {
        //type에 따라 이름, 전화번호, 그룹검색 나뉘어진다.
        super("Phone Book System :" + type + " Search Controller :)");
        pack();
        setContentPane(rootPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //이 JFrame만 close 해줌.

        this.SearchType = type;
        //type에 따라 이름, 전화번호, 그룹검색 나뉘어진다.

        setTable();
        init();
        stopflag = false;

        setSize(460, 500);
        setVisible(true);

    }

    private void setTable() {
        userColumn = new Vector<>();
        userColumn.add("Name");
        userColumn.add("Number");
        userColumn.add("Group");

        model = new DefaultTableModel(userColumn, 0);
        //table model create

        table1.setModel(model);

//        model.setRowCount(0);
        //clear table
//        model.fireTableDataChanged();

    }

    private void init() {
        mSearchCTRL = new SearchCTRL();

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("search clicked");
                model.setRowCount(0);
                model.fireTableDataChanged();

                if (SearchType.equals("Name")) {
                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            while (!stopflag && checkInput()) {
                                String sname = text_search.getText();
                                ArrayList<Vector<String>> result = mSearchCTRL.SearchName(sname);
                                if (result == null) {
                                    JOptionPane.showMessageDialog(null, "Search Result is null");
                                    stop();
//                                    interrupt();
                                }
                                for (int i = 0; i < result.size(); i++) {
                                    if (stopflag) {
                                        JOptionPane.showMessageDialog(null, "Search Stop");
                                        stop();
                                    }

                                    Vector<String> userRow = result.get(i);
                                    model.addRow(userRow);
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                JOptionPane.showMessageDialog(null, "Search Finished !");
                                stop();
//                                interrupt();
                            }
                        }
                    };
                    th.start();

                } else if (SearchType.equals("Number")) {
                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            while (!stopflag && checkInput()) {
                                String snumber = text_search.getText();

                                ArrayList<Vector<String>> result = mSearchCTRL.SearchNumbers(snumber);
                                if (result == null) {
                                    JOptionPane.showMessageDialog(null, "Search Result is null");
                                    stop();
//                                    interrupt();
                                }
                                for (int i = 0; i < result.size(); i++) {
                                    if (stopflag) {
                                        JOptionPane.showMessageDialog(null, "Search Stop");
                                        stop();
                                    }

                                    Vector<String> userRow = result.get(i);
                                    model.addRow(userRow);
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                JOptionPane.showMessageDialog(null, "Search Finished !");
                                stop();
//                                interrupt();
                            }
                        }
                    };
                    th.start();

                } else if (SearchType.equals("Group")) {
                    Thread th = new Thread() {
                        @Override
                        public void run() {
                            while (!stopflag) {
                                String sgroup = text_search.getText();
//                                mSearchCTRL.SearchName(sgroup);
                                ArrayList<Vector<String>> result = mSearchCTRL.SearchGroup(sgroup);
                                if (result == null) {
                                    JOptionPane.showMessageDialog(null, "Search Result is null");
                                    stop();
//                                    interrupt();
                                }

                                for (int i = 0; i < result.size(); i++) {
                                    if (stopflag) {
                                        JOptionPane.showMessageDialog(null, "Search Stop");
                                        stop();
                                    }

                                    Vector<String> userRow = result.get(i);
                                    model.addRow(userRow);
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                JOptionPane.showMessageDialog(null, "Search Finished !");
                                stop();
//                                interrupt();
                            }
                        }
                    };
                    th.start();

                }
            }
        });
        //삭제 //여러개..?
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("delete clicked");
                int row = table1.getSelectedRow();
                int column = table1.getSelectedColumn();

                Object value = table1.getValueAt(row, column);
                debugPrint(row + " " + column);
                debugPrint(value.toString());
                String name = table1.getValueAt(row, 0).toString();
                String number = table1.getValueAt(row, 1).toString();
                String group = table1.getValueAt(row, 2).toString();
                ContactsModel del = null;
                try {
                    del = new ContactsModel(name, number, group);
                } catch (FormatNotFoundException | CharacterExcessException | InputNotFoundException e1) {
                }
                mSearchCTRL.Delete(del);
                JOptionPane.showMessageDialog(null, "Delete Finished");

            }
        });
        //검색 취소
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("cancel clicked");
                stopflag = true;
            }
        });
        //연락처 수정 //한개씩만 가능
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (table1.isEditing()) {

                }
            }
        });
        modifyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("modify clicked");
                int row = table1.getSelectedRow();
                int column = table1.getSelectedColumn();
                Object value = table1.getValueAt(row, column);
                debugPrint(row + " " + column);
                debugPrint(value.toString());
                String name = table1.getValueAt(row, 0).toString();
                String number = table1.getValueAt(row, 1).toString();
                String group = table1.getValueAt(row, 2).toString();
                ContactsModel before = null;
                try {
                    before = new ContactsModel(name, number, group);
                } catch (FormatNotFoundException | CharacterExcessException | InputNotFoundException e1) {
                }

                Modify modifyFrame = new Modify("All",before);

            }
        });
        //선택된 여러개 SMS 전송
        sendSMSButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                debugPrint("sendSNS clicked");
                String text = textArea1.getText();
                //보낼 문자
                ArrayList<ContactsModel> list = mSearchCTRL.getResList();
                if (list.size() <= 0) {
                    JOptionPane.showMessageDialog(null, "Search Result is null. You can't send sms");
                }

                for (int i = 0; i < list.size(); i++) {
                    String resultText = list.get(i).GetNumber() + "\n";
                    resultText += text;
                    JOptionPane.showMessageDialog(null, resultText);
                }
            }
        });
    }

    //검색 input 검사
    private boolean checkInput() {
        if (SearchType.equals("Name")) {
            //이름 포맷이 다르면, Excetion throw
            String name = text_search.getText();
            Pattern p = Pattern.compile("[\\x{ac00}-\\x{d7af}]{1,4}");
            Matcher m = p.matcher(name);
            if (m.matches()) {
                return true;
            } else {
                try {
                    throw new FormatNotFoundException("Name");
                } catch (FormatNotFoundException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
//                    e.printStackTrace();
                }
            }

        } else if (SearchType.equals("Number")) {
            //전화번호에 문자가 있으면 Exception throw
            String number = text_search.getText();
            Pattern p = Pattern.compile("[\\d]{2,11}");
            Matcher m = p.matcher(number);
            if (m.matches()) {
                return true;
            } else {
                try {
                    throw new FormatNotFoundException("Number");
                } catch (FormatNotFoundException e) {
                    JOptionPane.showMessageDialog(null, e.getMessage());
//                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    private void print(ArrayList<Vector<String>> result) {
        for (int i = 0; i < result.size(); i++) {

            Vector<String> userRow = result.get(i);
            model.addRow(userRow);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void debugPrint(String str) {
        System.err.println("DEBUG:: " + str);
    }

}
