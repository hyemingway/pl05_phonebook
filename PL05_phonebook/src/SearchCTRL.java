import javax.swing.*;
import java.util.*;

/**
 * Created by DELL on 2015-05-31.
 */
//이름, 전화번호, 그룹명 검색 담당
//연락처 수정, 삭제

//InputNotFoundException : 검색어를 빈칸으로 두거나,
//FormatNotFoundException: 형식에 맞지 않는 내용일 경우,
// 이름에 기호나 숫자, 전화번호에 문자.

public class SearchCTRL {
    private ArrayList<ContactsModel> resList;
    private FileCTRL mFileCTRL;
    private GroupManage mGroupManage;
    private ArrayList<ContactsModel> list;
    private boolean stopflag;

    //const
    public SearchCTRL(){
        mFileCTRL = FileCTRL.getInstance();
        mGroupManage = GroupManage.getInstance();

        resList = new ArrayList<>();
        list = mFileCTRL.getmList();
        stopflag = false;
    }
    //Search
    public synchronized ArrayList<Vector<String>> SearchName(String sName){
        ArrayList<Vector<String>> result = new ArrayList<>();
        resList.clear();
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).GetName().contains(sName)){
                ContactsModel e = list.get(i);
                Vector<String> element = new Vector<>();
                element.add(e.GetName());
                element.add(e.GetNumber());
                element.add(e.GetGroup());
                result.add(element);
                resList.add(e);
                Thread.yield();
            }
        }

        return result;
    }
    public synchronized ArrayList<Vector<String>> SearchNumbers(String sNumber){
        ArrayList<Vector<String>> result = new ArrayList<>();
        resList.clear();
        for(int i = 0; i < list.size(); i++){
            if (list.get(i).GetRawNumber().contains(sNumber)){
                ContactsModel e = list.get(i);
                Vector<String> element = new Vector<>();
                element.add(e.GetName());
                element.add(e.GetNumber());
                element.add(e.GetGroup());
                result.add(element);
                resList.add(e);
                Thread.yield();
            }
        }
        return result;

    }
    public synchronized ArrayList<Vector<String>> SearchGroup(String sGroup){
        //해당 그룹 모두를 출력
        ArrayList<Vector<String>> result = new ArrayList<>();
        resList.clear();
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).GetGroup().contains(sGroup)){
                ContactsModel e = list.get(i);
                Vector<String> element = new Vector<>();
                element.add(e.GetName());
                element.add(e.GetNumber());
                element.add(e.GetGroup());
                result.add(element);
                resList.add(e);
                Thread.yield();
            }
        }
        return result;

    }
    public void StopSearch(){
//        thread.interrupt();
//        stopflag = true;
    }

    //Delete 연락처 삭제
    public synchronized void Delete(ContactsModel del){
        mFileCTRL.DeleteElement(del);

    }

    //검색 결과를 출력할 string
    public String resultStirng(){
        String resultString = "";
        for(int i = 0; i < resList.size(); i++){

        }
        return resultString;
    }

    public ArrayList<ContactsModel> getResList(){
        return resList;
    }
}
