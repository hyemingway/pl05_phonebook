/**
 * Created by DELL on 2015-05-31.
 */
public class FormatNotFoundException extends Exception {
    public FormatNotFoundException(String str){
        //str = number
        //str = name
        super(str + "Format is not adaptable.");
    }
}
