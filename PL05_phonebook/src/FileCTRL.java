import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
/**
 * Created by DELL on 2015-05-31.
 */
public class FileCTRL {
    private ArrayList<ContactsModel> mList;
    private PrintWriter pw = null;

    private static FileCTRL SingleFileCTRL;

    private FileCTRL(){
        try {
            mList = new ArrayList<>();
            pw = new PrintWriter(new FileWriter("data.txt", true));
            this.LoadFromFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Singleton Pattern
    public static FileCTRL getInstance(){
        if(SingleFileCTRL == null){
            SingleFileCTRL = new FileCTRL();
        }
        return SingleFileCTRL;
    }

    private synchronized void LoadFromFile(){
        ArrayList<ContactsModel> templist = new ArrayList<>();
        String headerText = "�̸�\t��ȭ��ȣ\t�׷�";
        //���Ϸκ��� �д´�.
        BufferedReader br = null;
        try {
            File file = new File("data.txt");
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"euc-kr"));
//            br.readLine();
            while (true) {
                String line = null;
                try {
                    line = br.readLine();
                    if (line == null) break;
                    if (line.equals(headerText)) continue;
                    ContactsModel temp = Tokenize(line);
                    if(temp == null) break;
//                    System.err.println(temp.toString());
                    templist.add(temp);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //�о� �� ������ templist�� �ִ´�.
        //templist -> mList
        mList.clear();
        for(ContactsModel cm : templist){
            mList.add(cm);
        }


    }

    private ContactsModel Tokenize(String line) {
        ContactsModel cm = null;
        String[] tokens = line.split("\t");
        if(tokens.length == 3){
            try {
                cm = new ContactsModel(tokens[0], tokens[1], tokens[2]);
            } catch (FormatNotFoundException | CharacterExcessException | InputNotFoundException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        }
        return cm;
    }

    private synchronized void SaveToFile(){
        //���Ͽ� List ������ ����
        PrintWriter pw = null;
        try {
            pw = new PrintWriter("data.txt");
            for (ContactsModel cm : mList) {
                String data = cm.toString();    //name  number  group
                pw.println(data);   // print line
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        LoadFromFile();
    }


    public ArrayList<ContactsModel> getmList(){
        return mList;
    }

    public void AddElement(ContactsModel cm){
        //����ó ���
        mList.add(cm);
        this.SaveToFile();
        this.LoadFromFile();
    }

    public void DeleteElement(ContactsModel cm){
        //����ó ����
        int idx = -1;
        for(int i = 0; i < mList.size(); i++){
            if(mList.get(i).GetName().equals(cm.GetName())){
                idx = i;
                break;
            }
        }
        if(idx != -1){
            mList.remove(idx);
        }
        this.SaveToFile();
        this.LoadFromFile();
    }

    public synchronized void ModifyElement(ContactsModel before, ContactsModel after){
        //����ó ����
        for(int i = 0; i < mList.size(); i++){
//            mList.contains(before);
            if(mList.get(i).GetName().equals(before.GetName())){
                int idx = i;
                mList.set(idx, after);
                System.err.println(mList.get(idx).toString());

            }
        }
//        if (mList.contains(before)) {
//            int idx = mList.indexOf(before);
//            mList.set(idx, after);
//            System.err.println(mList.get(idx).toString());
//        }
        this.SaveToFile();
        this.LoadFromFile();
    }

    public synchronized void ModifyGroup(String beforeGroup, String afterGroup){


        this.SaveToFile();
        this.LoadFromFile();

    }

    public synchronized void ModifyElement(int idx, ContactsModel after){
        mList.set(idx, after);
        System.err.println(mList.get(idx).toString());
        this.SaveToFile();
        this.LoadFromFile();
    }
    public void DeleteModifyGroup(ContactsModel cm){
        if(mList.contains(cm)){
            cm.GetGroup();
        }
    }
//    public synchronized void ReadFromList(){
//
//    }
}
