import sun.security.jca.GetInstance;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DELL on 2015-06-05.
 */
public class Modify extends JFrame{
    private JPanel rootPanel;
    private JButton commitButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JLabel show;
    private String name;
    private String number;
    private String group;
    private String modType;

    private FileCTRL mFileCTRL;
    private ContactsModel before;

    public Modify(String type, ContactsModel sbefore){
        //type에 따라 이름, 전화번호, 그룹검색 나뉘어진다.
        super("Phone Book System :)");
        pack();
        setContentPane(rootPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        //이 JFrame만 close 해줌.


        init();

        setSize(352, 400);
        setVisible(true);

        name = "";
        number = "";
        group = "";
        modType = type;
        this.before = sbefore;
        this.show.setText(sbefore.toString());

        mFileCTRL = FileCTRL.getInstance();

    }

    public void init(){
        commitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ContactsModel after = GetModel();
                    mFileCTRL.ModifyElement(before, after);

                } catch (InputNotFoundException | CharacterExcessException | FormatNotFoundException e1) {
                 JOptionPane.showMessageDialog(null, e1.getMessage());
                }
                JOptionPane.showMessageDialog(null, "Commit Finished");
            }
        });

    }

    public ContactsModel GetModel() throws InputNotFoundException, CharacterExcessException, FormatNotFoundException {
        ContactsModel cm = null;
        if(modType.equals("All")){
            name = textField1.getText();
            number = textField2.getText();
            group = textField3.getText();

            cm = new ContactsModel(name, number, group);

        }else if(modType.equals("Group")){
            name = before.GetName();
            number = before.GetNumber();
            group = textField3.getText();

            cm = new ContactsModel(name, number, group);
        }

        return cm;
    }

}
