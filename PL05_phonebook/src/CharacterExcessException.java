/**
 * Created by DELL on 2015-05-31.
 */
public class CharacterExcessException extends Exception {
    //이름 또는 그룹명이 5자를 넘어가는 경우
    public CharacterExcessException(){
        super("Please enter less than 5 characters :)");
    }
}
